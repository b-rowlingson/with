With <- function(..., code = {}){
    code = substitute(code)
    H = list(...)
    if(length(H)!=1){
        stop("Only one handler allowed")
    }
    N = names(H)
    if(nchar(N)<1){
        stop("Handler not assigned")
    }
    h = H[[1]]
    V = h$enter()

    e = new.env()
    assign(N, V, envir=e)
    tryCatch(
    {
        eval(code, envir=e)
    },
        finally=h$exit()
    )

}


indir <- function(d, debug=FALSE){
    dir = getwd()
    enter = function(){
        if(debug){message("enter")}
        setwd(d)
    }
    exit = function(){
        if(debug){message("exit")}
        setwd(dir)
    }
    list(enter=enter, exit=exit)
}


openfile <- function(f, debug=FALSE){
    tmp = list()
    enter = function(){
        if(debug){message("opening  file")}
        tmp[[1]] <<- file(f)
        return(tmp[[1]])
    }
    exit = function(){
        if(debug){message("closing file")}
        close(tmp[[1]])
    }
    list(enter=enter, exit=exit)
}
